//
//  JAHEmbedderViewController.m
//  PullToRefresh
//
//  Created by Jon Hjelle on 3/14/14.
//  Copyright (c) 2014 Jon Hjelle. All rights reserved.
//

#import "JAHEmbedderViewController.h"
#import "JAHEmbeddedViewController.h"

@interface JAHEmbedderViewController ()

@end

@implementation JAHEmbedderViewController

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"JAH_embedding"]) {
        JAHEmbeddedViewController* destVC = (JAHEmbeddedViewController*)segue.destinationViewController;
        destVC.embedderVC = self;
    }
}

@end
