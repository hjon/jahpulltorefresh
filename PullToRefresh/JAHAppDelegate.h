//
//  JAHAppDelegate.h
//  PullToRefresh
//
//  Created by Jon Hjelle on 3/4/14.
//  Copyright (c) 2014 Jon Hjelle. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface JAHAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
