//
//  JAHRefreshControl.h
//  PullToRefresh
//
//  Created by Jon Hjelle on 3/12/14.
//  Copyright (c) 2014 Jon Hjelle. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface JAHRefreshControl : UIControl

- (id)initWithTableView:(UITableView*)tableView atTop:(BOOL)atTop;

- (void)tearDownRefreshView;

- (void)viewWillLayoutSubviews;
- (void)scrollViewDidScroll:(UIScrollView *)scrollView;
- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView;
- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate;

@end
