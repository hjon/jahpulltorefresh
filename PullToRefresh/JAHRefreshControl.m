//
//  JAHRefreshControl.m
//  PullToRefresh
//
//  Created by Jon Hjelle on 3/12/14.
//  Copyright (c) 2014 Jon Hjelle. All rights reserved.
//

#import "JAHRefreshControl.h"

@interface JAHRefreshControl ()
@property (nonatomic, weak) UITableView* tableView;
@property (nonatomic, getter = isAtTop) BOOL atTop;
@property (nonatomic, getter = isUserInitiated) BOOL userInitiated;
@property (nonatomic, getter = isTriggered) BOOL triggered;
@property (nonatomic, getter = isCancelled) BOOL cancelled;
@property (nonatomic, strong) NSOperationQueue* operations;
@property (nonatomic, strong) CAShapeLayer* circleLayer;
@property (nonatomic, strong) CABasicAnimation* fillAnimation;
@end

@implementation JAHRefreshControl

- (id)initWithTableView:(UITableView*)tableView atTop:(BOOL)atTop {
    self = [super initWithFrame:CGRectZero];
    if (self) {
        self.tableView = tableView;
        self.atTop = atTop;
        [self setupLayers];
    }
    return self;
}

- (void)setupLayers {
    self.circleLayer = [CAShapeLayer layer];
    self.circleLayer.bounds = CGRectMake(0.0, 0.0, 30.0, 30.0);
    self.circleLayer.path = [[UIBezierPath bezierPathWithRect:CGRectMake(15.0f, 15.0f, 0.0f, 0.0f)] CGPath];
    self.circleLayer.fillColor = [[UIColor clearColor] CGColor];
    self.circleLayer.strokeColor = [[UIColor greenColor] CGColor];
    self.circleLayer.lineWidth = self.circleLayer.frame.size.width;
    [self.layer addSublayer:self.circleLayer];
    // Rotate so the 'fill' starts from the top of the circle
    self.circleLayer.transform = CATransform3DMakeRotation(-M_PI_2, 0.0, 0.0, 1.0);

    self.fillAnimation = [CABasicAnimation animationWithKeyPath:@"strokeEnd"];
    self.fillAnimation.duration = 1.0;
    self.fillAnimation.fromValue = @(0.0);
    self.fillAnimation.toValue = @(1.0);
    self.fillAnimation.fillMode = kCAFillModeForwards;
    [self.circleLayer addAnimation:self.fillAnimation forKey:@"animateStroke"];
    [self.circleLayer setSpeed:0.0];

    self.operations = [[NSOperationQueue alloc] init];
    [self.operations setMaxConcurrentOperationCount:1];
    [self.operations setSuspended:YES];
}

- (void)viewWillLayoutSubviews {
    if (!self.triggered) {
        if ([self isAtTop]) {
            [self setCenter:CGPointMake(self.tableView.backgroundView.center.x, CGRectGetMinY(self.tableView.backgroundView.bounds) + self.tableView.contentInset.top + 30.0)];
        } else {
            [self setCenter:CGPointMake(self.tableView.backgroundView.center.x, CGRectGetMaxY(self.tableView.backgroundView.bounds) - self.tableView.contentInset.bottom - 30.0)];
        }
    }
}

- (void)layoutSublayersOfLayer:(CALayer *)layer {
    if (layer == self.layer) {
        self.circleLayer.position = CGPointMake(CGRectGetMidX(self.layer.bounds), CGRectGetMidY(self.layer.bounds));

        self.circleLayer.frame = self.layer.bounds;
        self.circleLayer.path = [[UIBezierPath bezierPathWithOvalInRect:self.circleLayer.bounds] CGPath];
        self.circleLayer.lineWidth = self.circleLayer.frame.size.width;

        [self.circleLayer setMask:nil];

        CAShapeLayer* maskLayer = [CAShapeLayer layer];
        maskLayer.frame = self.circleLayer.frame;
        maskLayer.path = [[UIBezierPath bezierPathWithOvalInRect:self.circleLayer.bounds] CGPath];
        maskLayer.fillColor = [[UIColor colorWithWhite:1.0 alpha:1.0] CGColor];
        [self.circleLayer setMask:maskLayer];
    }
}

#define TRIGGER_POINT 60.0f

- (void)tearDownRefreshView {
    [self.operations addOperationWithBlock:^{
        dispatch_async(dispatch_get_main_queue(), ^{
            if ([self isAtTop]) {
                [UIView animateWithDuration:0.2 animations:^{
                    UIEdgeInsets insets = self.tableView.contentInset;
                    insets.top -= TRIGGER_POINT;
                    self.tableView.contentInset = insets;
                }];
            } else {
                [UIView animateWithDuration:0.2 animations:^{
                    UIEdgeInsets insets = self.tableView.contentInset;
                    insets.bottom -= TRIGGER_POINT;
                    self.tableView.contentInset = insets;
                }];
            }

            [self.circleLayer setSpeed:0.0];
            [self.circleLayer setTimeOffset:0.0];
            [self.circleLayer addAnimation:self.fillAnimation forKey:@"animateFill"];
            [self.circleLayer removeAnimationForKey:@"pulse"];

            [CATransaction begin];
            [CATransaction setValue:@(YES) forKey:kCATransactionDisableActions];
            [self.circleLayer setStrokeEnd:0.0];
            [CATransaction commit];
            
            _triggered = NO;
        });
    }];
}

#pragma mark - UIScrollViewDelegate methods

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    if (![self isUserInitiated] && ![self isCancelled]) {
        return;
    }

    if ([self isAtTop]) {
        if (!_triggered) {
            CGFloat offsetFromTop = -(scrollView.contentOffset.y + self.tableView.contentInset.top);
            [self.circleLayer setTimeOffset:(offsetFromTop / TRIGGER_POINT)];
        }
    } else {
        NSLog(@"For lots of content: %f >= %f", (scrollView.contentSize.height + scrollView.contentInset.top + scrollView.contentInset.bottom), scrollView.bounds.size.height);
        NSLog(@"If so... %f > %f", (scrollView.contentOffset.y + scrollView.bounds.size.height - scrollView.contentInset.bottom - scrollView.contentSize.height), TRIGGER_POINT);
        NSLog(@"Otherwise: %f > %f", (scrollView.contentOffset.y + scrollView.contentInset.top), TRIGGER_POINT);
        // We have to handle two different situations:
        // 1. A lot of rows (the total height of rows plus the contentInsets are taller than what can be shown on screen)
        // 2. Few to no rows (the total height of rows plus the contentInsets completely fit on screen)
        if ((scrollView.contentSize.height + scrollView.contentInset.top + scrollView.contentInset.bottom) >= scrollView.bounds.size.height) {
            CGFloat offsetFromBottom = scrollView.contentOffset.y + scrollView.bounds.size.height - scrollView.contentInset.bottom - scrollView.contentSize.height;
            NSLog(@"offsetFromBottom: %f", offsetFromBottom);
            if (!_triggered) {
                [self.circleLayer setTimeOffset:(offsetFromBottom / TRIGGER_POINT)];
            }
        } else {
            CGFloat offsetFromBottom = scrollView.contentOffset.y + scrollView.contentInset.top;
            if (!_triggered) {
                [self.circleLayer setTimeOffset:(offsetFromBottom / TRIGGER_POINT)];
            }
        }
    }
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
    [_operations setSuspended:YES];

    self.userInitiated = YES;
    self.cancelled = NO;
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
    if ([self isUserInitiated]) {
        BOOL firstTrigger = NO;

        if ([self isAtTop]) {
            CGFloat offsetFromTop = -(scrollView.contentOffset.y + self.tableView.contentInset.top);
            if (offsetFromTop >= TRIGGER_POINT) {
                if (!_triggered) {
                    _triggered = YES;
                    firstTrigger = YES;
                }
            } else if (offsetFromTop > 0.0) {
                self.cancelled = YES;
            }
        } else {
            NSLog(@"For lots of content: %f >= %f", (scrollView.contentSize.height + scrollView.contentInset.top + scrollView.contentInset.bottom), scrollView.bounds.size.height);
            NSLog(@"If so... %f > %f", (scrollView.contentOffset.y + scrollView.bounds.size.height - scrollView.contentInset.bottom - scrollView.contentSize.height), TRIGGER_POINT);
            NSLog(@"Otherwise: %f > %f", (scrollView.contentOffset.y + scrollView.contentInset.top), TRIGGER_POINT);
            // We have to handle two different situations:
            // 1. A lot of rows (the total height of rows plus the contentInsets are taller than what can be shown on screen)
            // 2. Few to no rows (the total height of rows plus the contentInsets completely fit on screen)
            if ((scrollView.contentSize.height + scrollView.contentInset.top + scrollView.contentInset.bottom) >= scrollView.bounds.size.height) {
                CGFloat offsetFromBottom = scrollView.contentOffset.y + scrollView.bounds.size.height - scrollView.contentInset.bottom - scrollView.contentSize.height;
                NSLog(@"offsetFromBottom: %f", offsetFromBottom);
                if (offsetFromBottom >= TRIGGER_POINT) {
                    if (!_triggered) {
                        _triggered = YES;
                        firstTrigger = YES;
                    }
                } else if (offsetFromBottom > 0.0) {
                    self.cancelled = YES;
                }
            } else {
                CGFloat offsetFromBottom = scrollView.contentOffset.y + scrollView.contentInset.top;
                if (offsetFromBottom >= TRIGGER_POINT) {
                    if (!_triggered) {
                        _triggered = YES;
                        firstTrigger = YES;
                    }
                } else if (offsetFromBottom > 0.0) {
                    self.cancelled = YES;
                }
            }
        }

        if (_triggered && firstTrigger) {
            if ([self isAtTop]) {
                [UIView animateWithDuration:0.2 animations:^{
                    UIEdgeInsets insets = self.tableView.contentInset;
                    insets.top += TRIGGER_POINT;
                    self.tableView.contentInset = insets;
                }];
            } else {
                [UIView animateWithDuration:0.2 animations:^{
                    UIEdgeInsets insets = self.tableView.contentInset;
                    insets.bottom += TRIGGER_POINT;
                    self.tableView.contentInset = insets;
                }];
            }

            CABasicAnimation* pulseAnimation = [CABasicAnimation animationWithKeyPath:@"strokeColor"];
            pulseAnimation.duration = 0.5;
            pulseAnimation.toValue = (id)[[UIColor colorWithHue:0.914 saturation:0.33 brightness:0.92 alpha:1.0] CGColor];
            pulseAnimation.autoreverses = YES;
            pulseAnimation.repeatCount = HUGE_VALF;

            [self.circleLayer addAnimation:pulseAnimation forKey:@"pulse"];
            [self.circleLayer setStrokeEnd:1.0];
            [self.circleLayer removeAnimationForKey:@"animateStroke"];
            [self.circleLayer setSpeed:1.0];

            [self sendActionsForControlEvents:UIControlEventValueChanged];

            firstTrigger = NO;
        }
    }

    self.userInitiated = NO;

    [_operations setSuspended:NO];
}

@end
