//
//  JAHTableViewController.m
//  PullToRefresh
//
//  Created by Jon Hjelle on 3/14/14.
//  Copyright (c) 2014 Jon Hjelle. All rights reserved.
//

#import "JAHTableViewController.h"

#import "JAHRefreshControl.h"

@interface JAHTableViewController ()
@property (nonatomic, strong) NSMutableArray* objects;
@property (nonatomic, strong) JAHRefreshControl* refresh;
@end

@implementation JAHTableViewController

#define AT_TOP NO

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UIBarButtonItem *addButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(insertNewObject:)];
    self.navigationItem.rightBarButtonItem = addButton;

    self.tableView.backgroundView = [[UIView alloc] init];
    self.tableView.backgroundView.backgroundColor = [UIColor lightGrayColor];

    // Not showing separators because using the bottom pull-to-refresh doesn't cooperate very well with them
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;

    self.refresh = [[JAHRefreshControl alloc] initWithTableView:self.tableView atTop:AT_TOP];
    [self.refresh addTarget:self action:@selector(triggerRefresh) forControlEvents:UIControlEventValueChanged];
    [self.refresh setFrame:CGRectMake(0.0f, 0.0f, 40.0f, 40.0f)];
    [self.tableView.backgroundView addSubview:self.refresh];
}

- (void)viewWillLayoutSubviews {
    [self.refresh viewWillLayoutSubviews];
}

- (void)insertNewObject:(id)sender {
    if (!self.objects) {
        self.objects = [[NSMutableArray alloc] init];
    }
    [self.objects insertObject:[NSDate date] atIndex:0];
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
    [self.tableView insertRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
}

#pragma mark - Refresh

- (void)triggerRefresh {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        sleep(10);
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.refresh tearDownRefreshView];
        });
    });
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.objects count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    
    NSDate *object = self.objects[indexPath.row];
    cell.textLabel.text = [object description];
    return cell;
}

#pragma mark - UIScrollViewDelegate methods

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    [self.refresh scrollViewDidScroll:scrollView];
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
    [self.refresh scrollViewWillBeginDragging:scrollView];
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
    [self.refresh scrollViewDidEndDragging:scrollView willDecelerate:decelerate];
}

@end
