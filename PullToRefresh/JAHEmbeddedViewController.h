//
//  JAHEmbeddedViewController.h
//  PullToRefresh
//
//  Created by Jon Hjelle on 3/14/14.
//  Copyright (c) 2014 Jon Hjelle. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface JAHEmbeddedViewController : UIViewController

@property (nonatomic, weak) UIViewController* embedderVC;

@end
