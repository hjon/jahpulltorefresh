//
//  main.m
//  PullToRefresh
//
//  Created by Jon Hjelle on 3/4/14.
//  Copyright (c) 2014 Jon Hjelle. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "JAHAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([JAHAppDelegate class]));
    }
}
